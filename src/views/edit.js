import { html } from '../../node_modules/lit-html/lit-html.js'
import { updateAlbum, getAlbumById } from '../api/data.js'

const editTemplate = (albumById, onSubmit) => {
  return html`<section id="edit">
    <div class="form">
      <h2>Edit Album</h2>
      <form class="edit-form" @submit=${onSubmit}>
        <input
          type="text"
          name="singer"
          id="album-singer"
          placeholder="Singer/Band"
          value=${albumById.singer}
        />
        <input
          type="text"
          name="album"
          id="album-album"
          placeholder="Album"
          value=${albumById.album}
        />
        <input
          type="text"
          name="imageUrl"
          id="album-img"
          placeholder="Image url"
          value=${albumById.imageUrl}
        />
        <input
          type="text"
          name="release"
          id="album-release"
          placeholder="Release date"
          value=${albumById.release}
        />
        <input
          type="text"
          name="label"
          id="album-label"
          placeholder="Label"
          value=${albumById.label}
        />
        <input
          type="text"
          name="sales"
          id="album-sales"
          placeholder="Sales"
          value=${albumById.sales}
        />

        <button type="submit">post</button>
      </form>
    </div>
  </section>`
}

export async function edit(ctx) {
  const albumId = ctx.params.id
  const albumById = await getAlbumById(albumId)
  ctx.render(editTemplate(albumById, onSubmit))

  async function onSubmit(e) {
    e.preventDefault()
    const formData = new FormData(e.target)
    const singer = formData.get('singer')
    const album = formData.get('album')
    const imageUrl = formData.get('imageUrl')
    const release = formData.get('release')
    const label = formData.get('label')
    const sales = formData.get('sales')

    if (!singer || !album || !imageUrl || !release || !label || !sales) {
      return alert('Fill out all fields!')
    }

    await updateAlbum(albumId, {
      singer,
      album,
      imageUrl,
      release,
      label,
      sales,
    })

    ctx.page.redirect('/details/' + albumId)
  }
}
