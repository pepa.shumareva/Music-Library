import { html } from '../../node_modules/lit-html/lit-html.js'
import { login } from '../api/data.js'

const loginTemplate = (onSubmit) => {
  return html` <section id="login">
    <div class="form">
      <h2>Login</h2>
      <form class="login-form" @submit=${onSubmit}>
        <input type="text" name="email" id="email" placeholder="email" />
        <input
          type="password"
          name="password"
          id="password"
          placeholder="password"
        />
        <button type="submit">login</button>
        <p class="message">Not registered? <a href="#">Create an account</a></p>
      </form>
    </div>
  </section>`
}

export async function loginPage(ctx) {
  ctx.render(loginTemplate(onSubmit))

  async function onSubmit(e) {
    e.preventDefault()
    const formData = new FormData(e.target)
    const email = formData.get('email').trim()
    const pass = formData.get('password').trim()

    if (!email || !pass) {
      return alert('Please fill out all of the fields!')
    }

    await login(email, pass)
    ctx.setUserNav()
    ctx.page.redirect('/dashboard')
  }
}
