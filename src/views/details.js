import { html } from '../../node_modules/lit-html/lit-html.js'
import { getAlbumById, deleteAlbum } from '../api/data.js'

const detailsTemplate = (albumById, isOwner, onDelete) => {
  return html` <section id="details">
    <div id="details-wrapper">
      <p id="details-title">Album Details</p>
      <div id="img-wrapper">
        <img src="${albumById.imageUrl}" alt="example1" />
      </div>
      <div id="info-wrapper">
        <p>
          <strong>Band:</strong
          ><span id="details-singer">${albumById.singer}</span>
        </p>
        <p>
          <strong>Album name:</strong
          ><span id="details-album">${albumById.album}</span>
        </p>
        <p>
          <strong>Release date:</strong
          ><span id="details-release">${albumById.release}</span>
        </p>
        <p>
          <strong>Label:</strong
          ><span id="details-label">${albumById.label}</span>
        </p>
        <p>
          <strong>Sales:</strong
          ><span id="details-sales">${albumById.sales}</span>
        </p>
      </div>
      <div id="likes">Likes: <span id="likes-count">0</span></div>

      ${isOwner
        ? html`<div id="action-buttons">
            <a href="" id="edit-btn">Edit</a>
            <a href="" id="delete-btn" @click=${onDelete}>Delete</a>
          </div>`
        : html`<div id="action-buttons">
            <a id="like-btn">Like</a>
          </div>`}
    </div>
  </section>`
}

export async function details(ctx) {
  const userId = sessionStorage.getItem('userId')
  const albumId = ctx.params.id
  const albumById = await getAlbumById(albumId)
  const isOwner = userId === albumById._ownerId
  ctx.render(detailsTemplate(albumById, isOwner, onDelete))

  async function onDelete(e) {
    e.preventDefault()
    const confirmation = confirm('Are you sure you want to delete this album?')
    if (confirmation) {
      await deleteAlbum(albumId)

      ctx.page.redirect('/dashboard')
    }
  }
}
