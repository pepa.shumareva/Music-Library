import { html } from '../../node_modules/lit-html/lit-html.js'
import { getAllAlbums } from '../api/data.js'

const detailsBtn = document.querySelector('.details-btn')

const dashboardTemplate = (albums) => {
  const availableAlbums = albums.length !== 0
  return html`<section id="dashboard">
    ${!availableAlbums
      ? html`<h2>There are no albums added yet.</h2>`
      : html`<h2>Albums</h2>
          <ul class="card-wrapper">
            ${albums.map(albumsTemplate)}
          </ul>`}
  </section>`
}

const albumsTemplate = (album) => {
  return html` <li class="card">
    <img src="${album.imageUrl}" alt="travis" />
    <p>
      <strong>Singer/Band: </strong><span class="singer">${album.singer}</span>
    </p>
    <p>
      <strong>Album name: </strong><span class="album">${album.album}</span>
    </p>
    <p><strong>Sales:</strong><span class="sales">${album.sales}</span></p>
    <a class="details-btn" href="/details/${album._id}">Details</a>
  </li>`
}

export async function dashboard(ctx) {
  const albums = await getAllAlbums()
  ctx.render(dashboardTemplate(albums))
}
