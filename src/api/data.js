import * as api from './api.js'
const host = 'http://localhost:3030'

export async function getAllAlbums() {
  const response = await fetch(host + '/data/albums?sortBy=_createdOn%20desc')
  const albums = await response.json()
  return albums
}

export async function createAlbum(album) {
  return await api.post(host + '/data/albums', album)
}

export async function getAlbumById(id) {
  return await api.get(host + '/data/albums/' + id)
}

export async function updateAlbum(id, album) {
  return await api.put(host + '/data/albums/' + id, album)
}

export async function deleteAlbum(id) {
  return await api.del(host + '/data/albums/' + id)
}

export const login = api.login
export const logout = api.logout
