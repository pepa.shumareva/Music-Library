import page from './node_modules/page/page.mjs'
import { render } from './node_modules/lit-html/lit-html.js'

import { homePage } from './src/views/homePage.js'
import { dashboard } from './src/views/dashboard.js'
import { create } from './src/views/create.js'
import { loginPage } from './src/views/login.js'
import { logout as apiLogout } from './src/api/data.js'
import { details } from './src/views/details.js'
import { edit } from './src/views/edit.js'

const main = document.querySelector('main')
setUserNav()
document.querySelector('.logoutBtn').addEventListener('click', logout)

page('/', decorateContext, homePage)
page('/dashboard', decorateContext, dashboard)
page('/create', decorateContext, create)
page('/login', decorateContext, loginPage)
page('/details/:id', decorateContext, details)
page('/edit/:id', decorateContext, edit)

page.start()

function decorateContext(ctx, next) {
  ctx.render = (content) => render(content, main)
  ctx.setUserNav = setUserNav
  next()
}

function setUserNav() {
  const email = sessionStorage.getItem(`email`)
  if (email !== null) {
    document.querySelector('.user').style.display = ''
    document.querySelector('.guest').style.display = 'none'
  } else {
    document.querySelector('.user').style.display = 'none'
    document.querySelector('.guest').style.display = ''
  }
}

async function logout() {
  await apiLogout()
  setUserNav()
  page.redirect('/dashboard')
}
